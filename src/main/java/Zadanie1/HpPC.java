package Zadanie1;

public class HpPC extends AbstractPC {
    public HpPC(String name, COMPUTER_BRAND computerBrand, int cpuPower, int gpuPower, boolean isOverclocked) {
        super(name, computerBrand, cpuPower, gpuPower, isOverclocked);
    }

    public static AbstractPC createH12(){
        return new HpPC("H12", COMPUTER_BRAND.HP, 1, 2, false);
    }

    public static AbstractPC createS42(){
        return new HpPC("S42", COMPUTER_BRAND.HP, 8, 4, true);
    }
}
