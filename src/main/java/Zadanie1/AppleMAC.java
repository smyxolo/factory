package Zadanie1;

public class AppleMAC extends AbstractPC {
    public AppleMAC(String name, int cpuPower, int gpuPower, boolean isOverclocked) {
        super(name, COMPUTER_BRAND.APPLE, cpuPower, gpuPower, isOverclocked);
    }

    public static AbstractPC createIMac(){
        return new AppleMAC("IMac", 12, 8, false);
    }

    public static AbstractPC createMacPRO(){
        return new AppleMAC("MacPRO", 22, 13, true);
    }
}
