package Zadanie1;

public class AsusPC extends AbstractPC {
    public AsusPC(String name, COMPUTER_BRAND computerBrand, int cpuPower, int gpuPower, boolean isOverclocked) {
        super(name, computerBrand, cpuPower, gpuPower, isOverclocked);
    }

    public static AbstractPC createECX15(){
        return new AsusPC("ECX15", COMPUTER_BRAND.ASUS, 3, 2, true);
    }

    public static AbstractPC createE25(){
        return new AsusPC("E25", COMPUTER_BRAND.ASUS, 2, 1, false);
    }
}
