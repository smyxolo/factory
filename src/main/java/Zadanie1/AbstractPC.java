package Zadanie1;

public class AbstractPC {
    private String name;
    private COMPUTER_BRAND computerBrand;
    private int cpuPower;
    private int gpuPower;
    private boolean isOverclocked;

    public AbstractPC(String name, COMPUTER_BRAND computerBrand, int cpuPower, int gpuPower, boolean isOverclocked) {
        this.name = name;
        this.computerBrand = computerBrand;
        this.cpuPower = cpuPower;
        this.gpuPower = gpuPower;
        this.isOverclocked = isOverclocked;
    }

    @Override
    public String toString() {
        return "AbstractPC{" +
                "name='" + name + '\'' +
                ", computerBrand=" + computerBrand +
                ", cpuPower=" + cpuPower +
                ", gpuPower=" + gpuPower +
                ", isOverclocked=" + isOverclocked +
                '}';
    }
}
