package Zadanie1;

public class SamsungPC extends AbstractPC {
    public SamsungPC(String name, COMPUTER_BRAND computerBrand, int cpuPower, int gpuPower, boolean isOverclocked) {
        super(name, computerBrand, cpuPower, gpuPower, isOverclocked);
    }

    public static AbstractPC createG52(){
        return new SamsungPC("G52", COMPUTER_BRAND.SAMSUNG, 3, 1, false);
    }

    public static AbstractPC createR62(){
        return new SamsungPC("R52", COMPUTER_BRAND.SAMSUNG, 9, 7, true);
    }
}
