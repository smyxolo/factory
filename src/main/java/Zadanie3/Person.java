package Zadanie3;

public class Person {
    private String imie;
    private String nazwisko;
    private int numerIndeksu;

    public Person(String imie, String nazwisko, int numerIndeksu) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.numerIndeksu = numerIndeksu;
    }

    @Override
    public String toString() {
        return "Person{" +
                "imie='" + imie + '\'' +
                ", nazwisko='" + nazwisko + '\'' +
                ", numerIndeksu=" + numerIndeksu +
                '}';
    }
}
