Zadanie  3: Stwórz  aplikację,  która j est  aplikacją  zgloszeniową  do  dziekanatu.  Wyobraźcie  sobie uczelnię,  która  posiada  system  webowy  do  dziekanatu.  Taki  system  pozwala  skladać zażalenia,  wnioski,  oraz r óżne i nne  pisma.  Czasami  problem j est t aki,  że  aby  wysłać wniosek t rzeba  wiedzieć  co  w  nim  napisać.  Ułatwimy t o  użytkownikowi  poprzez  stworzenie szablonów f ormularzy,  które  będą t worzone  przez  Fabrykę  Abstrakcyjną.
Stwórz  klasę  Application ( od  wniosek)  która  będzie r eprezentować  pojedynczy wniosek/pismo  które  możemy  złożyć  do  dziekanatu.  Klasa  powinna  nie  być  abstrakcyjna. Powinna  posiadać  pola:
-  dataUtworzenia ( LocalDateTime)
-  miejsceUtworzenia ( String) -  daneAplikanta ( Person) - t reść ( String)

Po t ej  klasie  powinny  dziedziczyć i nne  klasy ( umieść  klasę  Application  oraz  klasy dziedziczące  w j ednej/oddzielnej  paczce):  klasa  ConditionalStayApplication, SchoolarshipApplication,  SocialSchoolarshipApplication,  SemesterExtendApplication
Wszystkie  dziedziczą  po  klasie  Application.  Każdy  z  nich  posiada  konstruktor i : ConditionalStayApplication  –  posiada  dodatkowo  pole „ oceny(grades)” ( Lista l iczb t ypu double)  oraz  pole r eason ( powód)  –  String.
SchoolarshipApplication  –  posiada  dodatkowo  pole  grades ( tak  samo j ak  wyżej)  oraz extracurricularActivities ( zajęcia  dodatkowe)  które  są l istą  stringów.
SocialSchoolarshipApplication  –  posiada  pole  grades,  oraz t otalFamilyIncome ( double). SemesterExtendApplication  –  posiada  pole r eason ( String).
Stwórz f abrykę  abstrakcyjną  która  posiada  5  metod  Factory t worzących  obiekty i  wypełniając domyślne  pola t ych  klas.
Person j est  klasą  która  posiada  pola :  I mie,  nazwisko  oraz  numer i ndeksu. Stwórz  aplikację k tóra  pyta  o i mię,  nazwisko  oraz  numer i ndeksu  użytkownika, a  następnie  pyta j aki t yp f ormularza  chce  wypełnić,  następnie  prosi  o  dane  do  wypełnienia tego f ormularza.
np. Podaj i mię i  nazwisko,  oraz  numer i ndeksu: Paweł  Recław  00007
Jaki t yp f ormularza  chcesz  wypełnić (Warunek/StypendiumNaukowe/StypendiumSocjalne/WydluzenieSemestru)? StypendiumSocjalne
Podaj  swoje  oceny  oddzielone  przecinkami: 1,  2,  3,  5,  5,  2,  3,  3
Podaj  średni  dochód  na  członka r odziny: 50.15  zł
Dziękuję,  Twój f ormularz  został  wysłany. (Zakończ  aplikację)