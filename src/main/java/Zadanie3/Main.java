package Zadanie3;

import Zadanie3.Applications.Application;
import Zadanie3.Applications.ApplicationFactory;

import java.util.*;

public class Main {


    public static void main(String[] args) {
        Scanner skan = new Scanner(System.in);
        Person person = getPersonData(skan);
        Choice c = getFormType(skan);
        Application a;
        switch (c){
            case CONDITIONAL: {
                List<Double> grades = getGrades(skan);
                String reason = getReason(skan);
                a = ApplicationFactory.createConditionalStayApp(person, grades, reason);
                System.out.println(a);
                break;
            }
            case SCHOLARSHIP: {
                List<Double> grades = getGrades(skan);
                List<String> extraCurricular = getExtraCurricular(skan);
                a = ApplicationFactory.createScholarshipApp(person, grades, extraCurricular);
                System.out.println(a);
                break;
            }
            case EXTEND: {
                String reason = getReason(skan);
                a = ApplicationFactory.createExtendApp(person, reason);
                System.out.println(a);
                break;
            }
            case SOCIAL: {
                List<Double> grades = getGrades(skan);
                Integer familyIncome = getTotalFamilyIncome(skan);
                a = ApplicationFactory.createSocialApp(person, grades, familyIncome);
                System.out.println(a);
                break;
            }
        }

        System.out.println("Dziękujemy. Powyższy formularz został wysłany:");

    }

    private static Person getPersonData(Scanner skan){
        System.out.println("Podaj imie: ");
        String imie = skan.nextLine();
        System.out.println("Podaj nazwisko: ");
        String nazwisko = skan.nextLine();
        System.out.println("Podaj numer indeksu");
        int numerIndeksu = Integer.parseInt(skan.nextLine());
        return new Person(imie, nazwisko, numerIndeksu);
    }

    private static Choice getFormType(Scanner skan){
        System.out.println("Dostepne wnioski: Warunek/StypendiumNaukowe/WydluzenieSemestru/StypendiumSocjalne");
        System.out.println("Jaki rodzaj wniosku chcesz wypelnic?");
        String input = skan.nextLine();
        if(input.toLowerCase().contains("warunek")) return Choice.CONDITIONAL;
        else if (input.toLowerCase().contains("naukowe")) return Choice.SCHOLARSHIP;
        else if (input.toLowerCase().contains("wydluzenie")) return Choice.EXTEND;
        else if (input.toLowerCase().contains("socjalne")) return Choice.SOCIAL;
        else throw new IllegalArgumentException();
    }

    private static List<Double> getGrades(Scanner skan){
        System.out.println("Wpisz oceny oddzielone przecinkiem: ");
        List<Double> oceny = new ArrayList<>();
        String[] input = skan.nextLine().split(",");
        for (String s : input) {
            oceny.add(Double.parseDouble(s));
        }
        return oceny;
    }

    private static String getReason(Scanner skan){
        System.out.println("Podaj glowna motywacje wniosku:");
        return skan.nextLine();
    }

    private static List<String> getExtraCurricular(Scanner skan){
        System.out.println("Wpisz swoje aktywnosci dodatkowe oddzielajac je przecinkiem: ");
        return Arrays.asList(skan.nextLine().split(","));
    }

    private static int getTotalFamilyIncome(Scanner skan){
        System.out.println("Podaj dochód rodziny:");
        return Integer.parseInt(skan.nextLine());
    }


}
