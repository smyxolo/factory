package Zadanie3.Applications;

import Zadanie3.Person;

import java.time.LocalDate;
import java.util.List;

public class ConditionalStayApplication extends Application {
    private List<Double> grades;
    private String reason;

    public ConditionalStayApplication(LocalDate dataUtworzenia, String miejsceUtworzenia, Person daneAplikanta, String tresc, List<Double> grades, String reason) {
        super(dataUtworzenia, miejsceUtworzenia, daneAplikanta, tresc);
        this.grades = grades;
        this.reason = reason;
    }

    @Override
    public String toString() {
        return "ConditionalStayApplication{" +
                "grades=" + grades +
                ", reason='" + reason + '\'' +
                ", dataUtworzenia=" + dataUtworzenia +
                ", miejsceUtworzenia='" + miejsceUtworzenia + '\'' +
                ", daneAplikanta=" + daneAplikanta +
                ", tresc='" + tresc + '\'' +
                '}';
    }
}
