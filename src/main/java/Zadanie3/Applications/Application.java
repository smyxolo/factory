package Zadanie3.Applications;

import Zadanie3.Person;

import java.time.LocalDate;

public abstract class Application {
    LocalDate dataUtworzenia;
    String miejsceUtworzenia;
    Person daneAplikanta;
    String tresc;

    public Application(LocalDate dataUtworzenia, String miejsceUtworzenia, Person daneAplikanta, String tresc) {
        this.dataUtworzenia = dataUtworzenia;
        this.miejsceUtworzenia = miejsceUtworzenia;
        this.daneAplikanta = daneAplikanta;
        this.tresc = tresc;
    }

}
