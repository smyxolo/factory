package Zadanie3.Applications;

import Zadanie3.Person;

import java.time.LocalDate;
import java.util.List;

public class SocialScholarshipApplication extends Application {
    private List<Double> grades;
    private int totalFamilyIncome;

    public SocialScholarshipApplication(LocalDate dataUtworzenia, String miejsceUtworzenia, Person daneAplikanta, String tresc, List<Double> grades, int totalFamilyIncome) {
        super(dataUtworzenia, miejsceUtworzenia, daneAplikanta, tresc);
        this.grades = grades;
        this.totalFamilyIncome = totalFamilyIncome;
    }

    @Override
    public String toString() {
        return "SocialScholarshipApplication{" +
                "grades=" + grades +
                ", totalFamilyIncome=" + totalFamilyIncome +
                ", dataUtworzenia=" + dataUtworzenia +
                ", miejsceUtworzenia='" + miejsceUtworzenia + '\'' +
                ", daneAplikanta=" + daneAplikanta +
                ", tresc='" + tresc + '\'' +
                '}';
    }
}
