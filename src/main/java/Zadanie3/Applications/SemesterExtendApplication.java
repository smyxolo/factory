package Zadanie3.Applications;

import Zadanie3.Person;

import java.time.LocalDate;

public class SemesterExtendApplication extends Application {
    private String reason;

    public SemesterExtendApplication(LocalDate dataUtworzenia, String miejsceUtworzenia, Person daneAplikanta, String tresc, String reason) {
        super(dataUtworzenia, miejsceUtworzenia, daneAplikanta, tresc);
        this.reason = reason;
    }

    @Override
    public String toString() {
        return "SemesterExtendApplication{" +
                "reason='" + reason + '\'' +
                ", dataUtworzenia=" + dataUtworzenia +
                ", miejsceUtworzenia='" + miejsceUtworzenia + '\'' +
                ", daneAplikanta=" + daneAplikanta +
                ", tresc='" + tresc + '\'' +
                '}';
    }
}
