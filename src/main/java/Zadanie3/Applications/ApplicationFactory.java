package Zadanie3.Applications;

import Zadanie3.Person;

import java.time.LocalDate;
import java.util.List;

public abstract class ApplicationFactory {


    public static Application createConditionalStayApp(Person person, List<Double> grades, String reason){
        return new ConditionalStayApplication(LocalDate.now(), "Gdansk",  person, "Prosze o warunek.", grades, reason);
    }

    public static Application createScholarshipApp(Person person, List<Double> grades, List<String> extraCurricular){
        return new ScholarshipApplication(LocalDate.now(), "Gdansk", person, "Prosze o stypendium", grades, extraCurricular);
    }

    public static Application createExtendApp(Person person, String reason){
        return new SemesterExtendApplication(LocalDate.now(), "Gdansk", person, "Prosze o przedluzenie semestru", reason);
    }

    public static Application createSocialApp(Person person, List<Double> grades, int familyIncome){
        return new SocialScholarshipApplication(LocalDate.now(), "Gdansk", person, "Prosze o przedluzenie semestru", grades, familyIncome);
    }
}
