package Zadanie3.Applications;

import Zadanie3.Person;

import java.time.LocalDate;
import java.util.List;

public class ScholarshipApplication extends Application {
    private List<Double> grades;
    private List<String> extraCurricularActivities;

    public ScholarshipApplication(LocalDate dataUtworzenia, String miejsceUtworzenia, Person daneAplikanta, String tresc, List<Double> grades, List<String> extraCurricularActivities) {
        super(dataUtworzenia, miejsceUtworzenia, daneAplikanta, tresc);
        this.grades = grades;
        this.extraCurricularActivities = extraCurricularActivities;
    }

    @Override
    public String toString() {
        return "ScholarshipApplication{" +
                "grades=" + grades +
                ", extraCurricularActivities=" + extraCurricularActivities +
                ", dataUtworzenia=" + dataUtworzenia +
                ", miejsceUtworzenia='" + miejsceUtworzenia + '\'' +
                ", daneAplikanta=" + daneAplikanta +
                ", tresc='" + tresc + '\'' +
                '}';
    }
}
