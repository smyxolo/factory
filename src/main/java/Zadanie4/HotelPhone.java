package Zadanie4;

import lombok.Getter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
public class HotelPhone {
    private List<PhoneCall> callHistory = new ArrayList<>();
    private Map<SPEED_DIAL, Integer> speedDialList = fillSpeedDial();

    //todo room is empty, noone will answer...

    private Map<SPEED_DIAL,Integer> fillSpeedDial() {
        Map<SPEED_DIAL, Integer> speedDialList = new HashMap<>();
        speedDialList.put(SPEED_DIAL.SPEED_DIAL1, 1234);
        speedDialList.put(SPEED_DIAL.SPEED_DIAL2, 2345);
        speedDialList.put(SPEED_DIAL.SPEED_DIAL3, 3456);
        speedDialList.put(SPEED_DIAL.SPEED_DIAL4, 4567);
        speedDialList.put(SPEED_DIAL.MANAGER, 5678);
        return speedDialList;
    }

    public void call(HotelGuest guest, int number){
        System.out.println("Guest " + guest.getName() + " " + guest.getSurname() + " calling " + number + "...");
        callHistory.add(new PhoneCall(number, guest));
    }

    public void speedCall(SPEED_DIAL SPEEDDIAL){
        System.out.println("Calling " + SPEEDDIAL + " at " + speedDialList.get(SPEEDDIAL) + "...");
    }

    public void SetSpeedDial(SPEED_DIAL SPEEDDIAL, int number){
        speedDialList.put(SPEEDDIAL, number);
    }
}
