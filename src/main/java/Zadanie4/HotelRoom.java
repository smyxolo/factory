package Zadanie4;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class HotelRoom {
    private int roomNumber;
    public boolean isOccupied = false;
    private HotelGuest occupyingGuest;

    public HotelRoom(int roomNumber) {
        this.roomNumber = roomNumber;
    }

    //todo: assign numbers to rooms and sout rooms when calling/speedCalling
}
