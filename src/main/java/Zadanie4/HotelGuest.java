package Zadanie4;

import lombok.Getter;

@Getter

public class HotelGuest {
    private String name;
    private String surname;

    public HotelGuest(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    @Override
    public String toString() {
        return name +
                " " + surname;
    }
}
