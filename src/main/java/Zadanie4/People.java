package Zadanie4;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class People {

    private List<HotelGuest> potentialGuests;

    private static final People ourInstance = new People();

    public static People getInstance() {
        return ourInstance;
    }

    private People() {
        this.potentialGuests = fillWithPeople();
    }

    private List<HotelGuest> fillWithPeople(){
        List<HotelGuest> list = new ArrayList<>();
        list.add(new HotelGuest("Tomasz", "Knapik"));
        list.add(new HotelGuest("Olgierd", "Waryński"));
        list.add(new HotelGuest("Norbert", "Trzebiatowski"));
        list.add(new HotelGuest("Kamil", "Durczok"));
        list.add(new HotelGuest("Oniegin", "Wrypacz"));
        list.add(new HotelGuest("Bogumiła", "Nakłońska"));
        list.add(new HotelGuest("Leszek", "Trańczyk"));
        list.add(new HotelGuest("Maria", "Bąk"));
        list.add(new HotelGuest("Marta", "Krugły"));
        list.add(new HotelGuest("Katarzyna", "Gugniewicz"));
        list.add(new HotelGuest("Włodzimierz", "Szaramowicz"));
        return list;
    }

    public static HotelGuest getRandomGuest(){
        Random random = new Random();
        int guestIndex = random.nextInt(ourInstance.potentialGuests.size());
        return ourInstance.potentialGuests.get(guestIndex);
    }
}
