package Zadanie4;

public abstract class HotelCash {

    public static HotelReceipt generateHighReceipt(){
        return new HotelReceipt(RECEIPT_TYPE.HIGH);
    }

    public static HotelReceipt generateMidReceipt(){
        return new HotelReceipt(RECEIPT_TYPE.MID);
    }

    public static HotelReceipt generateLowReceipt(){
        return new HotelReceipt(RECEIPT_TYPE.LOW);
    }

    public static HotelReceipt generateReceipt(HotelRoom room){
        if(room.getRoomNumber() < 10){
            return new HotelReceipt(RECEIPT_TYPE.HIGH);
        }
        else if (room.getRoomNumber() < 20){
            return new HotelReceipt(RECEIPT_TYPE.MID);
        }
        else if (room.getRoomNumber() <= 30){
            return new HotelReceipt(RECEIPT_TYPE.LOW);
        }
        else throw new IllegalArgumentException();
    }
}
