package Zadanie4;

public class PhoneCall {
    private int number;
    private HotelGuest caller;

    public PhoneCall(int number, HotelGuest caller) {
        this.number = number;
        this.caller = caller;
    }

    @Override
    public String toString() {
        return "\n{" +
                "number=" + number +
                ", caller=" + caller +
                '}';
    }
}
