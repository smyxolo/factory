package Zadanie4;

import lombok.Getter;

import java.util.Random;

@Getter
public enum PROCEDURE {
    REGISTER(0, 39), UNREGISTER(40, 79), CALL(80, 94), COMPLAINT(95, 99);
    private int rangeMin;
    private int rangeMax;

    PROCEDURE(int rangeMin, int rangeMax) {
        this.rangeMin = rangeMin;
        this.rangeMax = rangeMax;
    }

    public static boolean isInRange(PROCEDURE procedure, int number){
        return (number >= procedure.getRangeMin() && number <= procedure.getRangeMax());
    }

    public static PROCEDURE randomProcedure(){
        Random random = new Random();
        int randomNumber = random.nextInt(100);
        if (isInRange(REGISTER, randomNumber)) return REGISTER;
        else if (isInRange(UNREGISTER, randomNumber)) return UNREGISTER;
        else if (isInRange(CALL, randomNumber)) return CALL;
        else if (isInRange(COMPLAINT, randomNumber)) return COMPLAINT;
        else throw new IllegalArgumentException();
    }
}
