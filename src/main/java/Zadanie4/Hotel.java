package Zadanie4;

import lombok.Getter;

import java.util.*;

@Getter


public class Hotel {
    private HotelPhone hotelPhone;
    private List<HotelEmployee> employeeList = fillEmployeeList();
    private List<HotelRoom> hotelRooms = new ArrayList<>();
    private List<HotelGuest> guestList = new ArrayList<>();
    private Double totalCash = 0.0d;

    public Hotel() {
        this.hotelPhone = new HotelPhone();
        this.employeeList = fillEmployeeList();
        this.hotelRooms = fillRoomList();
    }

    public void handleClient(HotelGuest hotelGuest) {
        HotelEmployee e = employeeList.get(0);
        for (HotelEmployee he : employeeList) {
            if (he.getHandledClients().size() < e.getHandledClients().size()) e = he;
        }
        e.getHandledClients().add(hotelGuest);
        System.out.println(hotelGuest.getName() + " " + hotelGuest.getSurname() + " is assisted by " + e);
    }

    public List<HotelEmployee> fillEmployeeList() {
        List<HotelEmployee> employeeList = new ArrayList<>();
        employeeList.add(new HotelEmployee("Tomasz", "Urbaniak"));
        employeeList.add(new HotelEmployee("Grzegorz", "Tomczyk"));
        employeeList.add(new HotelEmployee("Michał", "Pankau"));
        employeeList.add(new HotelEmployee("Andrzej", "Gradowski"));
        employeeList.add(new HotelEmployee("Brygida", "Olejnik"));
        employeeList.add(new HotelEmployee("Tamara", "Aleksjejeva"));
        employeeList.add(new HotelEmployee("Olgierd", "Brzeszczot"));
        employeeList.add(new HotelEmployee("Nina", "Oleander"));
        employeeList.add(new HotelEmployee("Helmut", "Kohl"));
        return employeeList;
    }

    public List<HotelRoom> fillRoomList() {
        List<HotelRoom> roomList = new ArrayList<>();
        for (int i = 1; i <= 30; i++) {
            roomList.add(new HotelRoom(i));
        }
        return roomList;
    }

    public HotelRoom getRandomRoom() {
        Random r = new Random();
        int roomIndex = r.nextInt(hotelRooms.size());
        return hotelRooms.get(roomIndex);
    }

    public void performRandomProcedure() {
        PROCEDURE p = PROCEDURE.randomProcedure();
        switch (p) {
            case REGISTER: {
                System.out.println("Registering new guest:");
                HotelGuest guest = People.getRandomGuest();
                registerNewGuest(guest);
                break;
            }

            case UNREGISTER: {
                if (!guestList.isEmpty()) {
                    System.out.println("Unregistering guest:");
                    HotelGuest hg = pickAGuest();
                    unregisterGuest(hg);
                }
                else System.out.println("Hotel is empty.");
                break;
            }

            case CALL: {
                System.out.println("Call:");
                Random random = new Random();
                HotelGuest hg = People.getRandomGuest();
                hotelPhone.call(hg, random.nextInt(100000000));
                break;
            }

            case COMPLAINT: {
                System.out.println("Complaint: ");
                hotelPhone.speedCall(SPEED_DIAL.MANAGER);
                break;
            }
        }
    }

    public HotelGuest identifyGuest(String name, String surname) {
        for (HotelRoom hotelRoom : hotelRooms) {
            if (hotelRoom.isOccupied
                    && hotelRoom.getOccupyingGuest().getName().equals(name)
                    && hotelRoom.getOccupyingGuest().getSurname().equals(surname)) {
                return hotelRoom.getOccupyingGuest();
            }
        }
        throw new IllegalArgumentException("Guest does not figure in the database.");
    }

    public void registerNewGuest(HotelGuest guest) {
        handleClient(guest);
        boolean guestRegistered = false;

        while (!guestRegistered) {
            HotelRoom hotelRoom = getRandomRoom();
            if (!hotelRoom.isOccupied) {
                hotelRoom.setOccupyingGuest(guest);
                hotelRoom.isOccupied = true;
                guestList.add(guest);
                System.out.println("Guest " + guest.getName() + " " + guest.getSurname() +
                        " is now residing in room " + hotelRoom.getRoomNumber() + ".");
                guestRegistered = true;
            }
        }
    }

    public void unregisterGuest(HotelGuest guest) {
        HotelRoom guestsRoom;
        for (HotelRoom hotelRoom : hotelRooms) {
            if (hotelRoom.isOccupied && hotelRoom.getOccupyingGuest().getName().equals(guest.getName())
                    && hotelRoom.getOccupyingGuest().getSurname().equals(guest.getSurname())) {
                guestList.remove(hotelRoom.getOccupyingGuest());
                hotelRoom.setOccupyingGuest(null);
                hotelRoom.setOccupied(false);
                System.out.println("Guest " + guest.getName() + " " + guest.getSurname() +
                        " has been unregistered from room number " + hotelRoom.getRoomNumber() + " .");
                HotelReceipt receipt = HotelCash.generateReceipt(hotelRoom);
                totalCash += receipt.getQuote();
                System.out.println("Receipt: " + receipt);
                break;
            }
        }

    }

    public void checkCalls() {
        System.out.println(hotelPhone.getCallHistory());
    }

    public void checkRooms() {
        Set<Integer> available = new HashSet<>();
        Set<Integer> occupied = new HashSet<>();
        for (HotelRoom hr : hotelRooms) {
            if (!hr.isOccupied) available.add(hr.getRoomNumber());
            else occupied.add(hr.getRoomNumber());
        }
        System.out.println(occupied.size() + " Occupied rooms: " + occupied);
        System.out.println(available.size() + " Available rooms: " + available);
    }

    public HotelGuest pickAGuest() {
        Random r = new Random();
        int randomGuestIndex = r.nextInt(guestList.size());
        return guestList.get(randomGuestIndex);
    }
}
