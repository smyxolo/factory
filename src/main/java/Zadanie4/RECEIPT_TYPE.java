package Zadanie4;

import lombok.Getter;

@Getter
public enum RECEIPT_TYPE {
    LOW(120.57), MID(150.78), HIGH(220.32);
    private double quote;

    RECEIPT_TYPE(double quote) {
        this.quote = quote;
    }
}
