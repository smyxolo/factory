package Zadanie4;

import lombok.Getter;

@Getter

public class HotelReceipt {
    private RECEIPT_TYPE type;
    private double quote;

    public HotelReceipt(RECEIPT_TYPE type) {
        this.type = type;
        this.quote = type.getQuote();
    }

    @Override
    public String toString() {
        return "HotelReceipt{" +
                "RoomStandard: " + type +
                ", quote to pay: " + quote +
                '}';
    }
}
