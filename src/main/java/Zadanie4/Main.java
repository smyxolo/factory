package Zadanie4;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Hotel hotel = new Hotel();
        Scanner skan = new Scanner(System.in);
        boolean isWorking = true;

            System.out.println("Enter command: register / unregister / internal (call) / external (call) /check rooms/ check calls/ random (r) / quit");
        while (isWorking) {
            String inputLine = skan.nextLine();
            if (inputLine.contains("quit")) {
                isWorking = false;
            } else if (inputLine.equals("random") || inputLine.equals("r")) {
                hotel.performRandomProcedure();
            } else if (inputLine.equals("register")) {
                HotelGuest guest = getGuestData(skan);
                hotel.registerNewGuest(guest);
            } else if (inputLine.equals("unregister")) {
                HotelGuest guest = getGuestData(skan);
                hotel.unregisterGuest(guest);
            }
            else if (inputLine.contains("external")){
                HotelGuest guest = getGuestData(skan);
                int number = getNumberToCall(skan);
                hotel.getHotelPhone().call(guest, number);
            }
            else if (inputLine.contains("internal")){
                SPEED_DIAL sd = getSpeedDial(skan);
                hotel.getHotelPhone().speedCall(sd);
            }
            else if (inputLine.contains("calls")){
                hotel.checkCalls();
            }
            else if (inputLine.contains("rooms")){
                hotel.checkRooms();
            }
            else if(inputLine.equals("cash")){
                System.out.println(hotel.getTotalCash());
            }

            else if (inputLine.equals("")){
                System.out.println("Enter command: register / unregister / internal (call) / external (call) /check rooms/ check calls/ random (r) / quit");
            }
            System.out.println();
        }
    }

    public static HotelGuest getGuestData(Scanner skan) {
        System.out.println("Podaj imie: ");
        String name = skan.nextLine();
        System.out.println("Podaj nazwisko: ");
        String surname = skan.nextLine();
        return new HotelGuest(name, surname);
    }

    public static int getNumberToCall(Scanner skan){
        System.out.println("Podaj numer, na ktory chcesz zadzwonic: ");
        return Integer.parseInt(skan.nextLine());
    }

    public static SPEED_DIAL getSpeedDial(Scanner skan){
        System.out.println("Wpisz numer szybkiego wybierania (od 1 do 4): ");
        String inputLine = skan.nextLine();
        switch (inputLine){
            case "1": return SPEED_DIAL.SPEED_DIAL1;
            case "2": return SPEED_DIAL.SPEED_DIAL2;
            case "3": return SPEED_DIAL.SPEED_DIAL3;
            case "4": return SPEED_DIAL.SPEED_DIAL4;
            default: throw new IllegalArgumentException("Wrong number entered.");
        }
    }
}
