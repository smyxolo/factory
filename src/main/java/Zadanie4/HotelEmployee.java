package Zadanie4;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
@Getter
public class HotelEmployee {
    private String name;
    private String surname;
    private List<HotelGuest> handledClients = new ArrayList<>();

    public HotelEmployee(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    @Override
    public String toString() {
        return "hotel employee " + name +
                " " + surname;
    }
}
