package Zadanie2;

public abstract class BikeFactory {
    public static Bike createKross(){
        return new Bike("Kross", 1, 5, BIKE_TYPE.BICYCLE);
    }

    public static Bike createMerida(){
        return new Bike("Merida", 1, 6, BIKE_TYPE.BICYCLE);
    }

    public static Bike createIniana(){
        return new Bike("Iniana", 2, 3, BIKE_TYPE.TANDEM);
    }

    public static Bike createFelt(){
        return new Bike("Felt", 1, 6, BIKE_TYPE.BICYCLE);
    }

    public static Bike createGoetze(){
        return new Bike("Goetze", 2, 1, BIKE_TYPE.TANDEM);
    }
}
