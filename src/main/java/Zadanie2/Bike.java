package Zadanie2;

public class Bike {
    private String brand;
    private int seats;
    private int gears;
    private BIKE_TYPE bike_type;

    public Bike(String brand, int seats, int gears, BIKE_TYPE bike_type) {
        this.brand = brand;
        this.seats = seats;
        this.gears = gears;
        this.bike_type = bike_type;
    }

    @Override
    public String toString() {
        return "Bike{" +
                "brand='" + brand + '\'' +
                ", seats=" + seats +
                ", gears=" + gears +
                ", bike_type=" + bike_type +
                '}';
    }
}
