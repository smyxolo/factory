package Zadanie2;

public class Main {
    public static void main(String[] args) {
        System.out.println(BikeFactory.createFelt());
        System.out.println(BikeFactory.createGoetze());
        System.out.println(BikeFactory.createIniana());
        System.out.println(BikeFactory.createKross());
        System.out.println(BikeFactory.createMerida());
    }
}
